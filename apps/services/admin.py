from django.contrib import admin

from .models import Category, Institution, InstitutionService, Service


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Institution)
class InstitutionAdmin(admin.ModelAdmin):
    pass


@admin.register(InstitutionService)
class InstitutionServiceAdmin(admin.ModelAdmin):
    pass


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    filter_horizontal = ('categories',)
