from rest_framework.routers import DefaultRouter

from .views import InstitutionServiceViewSet, PublicServiceViewSet, ServiceViewSet

router = DefaultRouter()
router.register(r'public', PublicServiceViewSet, basename='public-service')
router.register(r'available', ServiceViewSet, basename='available-service')
router.register(r'offered', InstitutionServiceViewSet, basename='offered-service')

urlpatterns = [
    *router.urls,
]
