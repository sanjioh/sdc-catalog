from rest_framework import pagination


class InstitutionServicePagination(pagination.PageNumberPagination):
    page_size = 20


class ServicePagination(pagination.PageNumberPagination):
    page_size = 20
