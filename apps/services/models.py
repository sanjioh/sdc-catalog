from django.conf import settings
from django.db import models
from django.utils.translation import gettext as _


class Institution(models.Model):
    name = models.CharField(_('name'), max_length=256)
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        verbose_name=_('user'),
        related_name='institution',
        on_delete=models.PROTECT,
    )
    services = models.ManyToManyField(
        'Service',
        verbose_name=_('services'),
        related_name='institutions',
        through='InstitutionService',
        blank=True,
    )

    def __str__(self):
        return self.name


class Service(models.Model):
    name = models.CharField(_('name'), max_length=256, unique=True)
    usage = models.PositiveIntegerField(_('usage'), default=0)
    categories = models.ManyToManyField('Category', blank=True)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(_('name'), max_length=64, unique=True)

    class Meta:
        verbose_name_plural = _('categories')

    def __str__(self):
        return self.name


class InstitutionService(models.Model):
    institution = models.ForeignKey('Institution', on_delete=models.CASCADE)
    service = models.ForeignKey('Service', on_delete=models.CASCADE)

    class Meta:
        constraints = [models.UniqueConstraint(fields=('institution', 'service'), name='unique_institution_service')]

    def __str__(self):
        return f'{self.institution.name} - {self.service.name}'
