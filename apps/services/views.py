from django.db import models
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework import permissions
from rest_framework.mixins import CreateModelMixin, DestroyModelMixin, ListModelMixin
from rest_framework.viewsets import GenericViewSet

from .models import InstitutionService, Service
from .pagination import InstitutionServicePagination, ServicePagination
from .serializers import InstitutionServiceSerializer, PublicServiceSerializer, ServiceSerializer


class ServiceViewSet(ListModelMixin, GenericViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ServiceSerializer
    pagination_class = ServicePagination
    queryset = Service.objects.order_by('id')


class InstitutionServiceViewSet(
    CreateModelMixin,
    ListModelMixin,
    DestroyModelMixin,
    GenericViewSet,
):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = InstitutionServiceSerializer
    pagination_class = InstitutionServicePagination

    def get_queryset(self):
        return InstitutionService.objects.filter(institution=self.request.user.institution).order_by('id')


class PublicServiceViewSet(ListModelMixin, GenericViewSet):
    permission_classes = (permissions.AllowAny,)
    serializer_class = PublicServiceSerializer
    pagination_class = ServicePagination
    queryset = (
        Service.objects.annotate(institution_count=models.Count('institutions'))
        .filter(institution_count__gt=0)
        .order_by('-usage')
    )

    @method_decorator(cache_page(10))
    def list(self, request, *args, **kwargs):  # noqa
        return super().list(request, *args, **kwargs)
