from rest_framework import serializers, validators

from .models import Institution, InstitutionService, Service


class CurrentInstitutionDefault(serializers.CurrentUserDefault):
    def __call__(self, serializer_field):
        return super().__call__(serializer_field).institution


class ServiceSerializer(serializers.ModelSerializer):
    categories = serializers.SlugRelatedField(many=True, read_only=True, slug_field='name')

    class Meta:
        model = Service
        fields = ('id', 'name', 'usage', 'categories')


class InstitutionServiceSerializer(serializers.ModelSerializer):
    institution = serializers.HiddenField(default=CurrentInstitutionDefault())

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['service'] = ServiceSerializer(instance=instance.service).data
        return representation

    class Meta:
        model = InstitutionService
        fields = ('id', 'institution', 'service')
        validators = [
            validators.UniqueTogetherValidator(
                queryset=InstitutionService.objects.all(),
                fields=['institution', 'service'],
            ),
        ]


class InstitutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Institution
        fields = ('name',)


class PublicServiceSerializer(serializers.ModelSerializer):
    institutions = InstitutionSerializer(many=True)
    categories = serializers.SlugRelatedField(many=True, read_only=True, slug_field='name')

    class Meta:
        model = Service
        fields = ('id', 'name', 'usage', 'institutions', 'categories')
