from django.db import connection
from rest_framework import permissions, response
from rest_framework.decorators import api_view, permission_classes


@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def healthcheck(request):
    with connection.cursor() as cursor:
        cursor.execute('SELECT 1')
    return response.Response()
