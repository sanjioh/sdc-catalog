import pytest
from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from apps.services.models import Category, Institution, InstitutionService, Service

User = get_user_model()


@pytest.fixture(autouse=True)
def enable_db_access_for_all_tests(db):
    pass


@pytest.fixture
def user():
    return User.objects.create_user(
        username='bugliano',
        password='password-secret',
    )


@pytest.fixture
def apiclient(user):
    apiclient = APIClient()
    token = Token.objects.create(user=user)
    apiclient.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')
    return apiclient


@pytest.fixture
def institution(user):
    return Institution.objects.create(
        name='Comune di Bugliano',
        user=user,
    )


@pytest.fixture
def service():
    service = Service.objects.create(name='TARI', usage=2)
    service.categories.set(Category.objects.create(name=name) for name in ('Tributi', 'Ambiente'))
    return service


def test_institution_services(apiclient, service, institution):
    institution_service = InstitutionService.objects.create(institution=institution, service=service)
    url = reverse('offered-service-list')
    response = apiclient.get(url)
    assert response.json() == {
        'count': 1,
        'next': None,
        'previous': None,
        'results': [
            {
                'id': institution_service.pk,
                'service': {
                    'id': service.pk,
                    'name': 'TARI',
                    'usage': 2,
                    'categories': ['Tributi', 'Ambiente'],
                },
            },
        ],
    }
